package org.bitbucket.rajibpsarma.example1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * In this application, H2 database is started in in-memory mode,
 * which reads schema.sql and data.sql files to create and populate the ROOMS table.
 * When we invoke the REST by using url: http://localhost:8080/rooms,
 * it displays the data in JSON format:
 * 
[{
	"roomNumber": 101,
	"roomName": "Room 101",
	"roomDescription": "Single room with attached bath"
}, {
	"roomNumber": 201,
	"roomName": "Room 201",
	"roomDescription": "Double room with attached bath"
}]
 * 
 * @author RSarma
 *
 */
@SpringBootApplication
public class Example1Application {

	public static void main(String[] args) {
		SpringApplication.run(Example1Application.class, args);
	}

}
