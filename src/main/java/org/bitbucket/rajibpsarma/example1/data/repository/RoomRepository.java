package org.bitbucket.rajibpsarma.example1.data.repository;

import org.bitbucket.rajibpsarma.example1.data.entity.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends CrudRepository<Room, Integer> {

}
