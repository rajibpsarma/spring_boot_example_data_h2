package org.bitbucket.rajibpsarma.example1.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ROOMS")
public class Room {
	@Id
	@Column(name="ROOM_NUMBER")
	private int roomNumber;
	
	@Column(name="ROOM_NAME")
	private String roomName;
	
	@Column(name="ROOM_DESC")
	private String roomDescription;
	
	public Room() {}
	
	public Room(int roomNumber, String roomName, String roomDescription) {
		super();
		this.roomNumber = roomNumber;
		this.roomName = roomName;
		this.roomDescription = roomDescription;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomDescription() {
		return roomDescription;
	}

	public void setRoomDescription(String roomDescription) {
		this.roomDescription = roomDescription;
	}
}
